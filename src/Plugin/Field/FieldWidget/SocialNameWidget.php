<?php

namespace Drupal\social_name_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'social_name' widget.
 *
 * @FieldWidget(
 *   id = "social_name",
 *   module = "social_name_field",
 *   label = @Translation("Social name"),
 *   field_types = {
 *     "social_name"
 *   }
 * )
 */
class SocialNameWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
    ];


    $element['social_url'] = [
      '#markup' => $this->getFieldSetting('social_url'),
    ];

    $element['social_name'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->social_name ?? NULL,
      '#size' => 30,
    ];

    $element['#element_validate'][] = [get_called_class(), 'validateSocialName'];

    return $element;
  }

  /**
   * Form element validation handler for the 'social_name' element.
   *
   * Check that a user input a valid social name.
   */
  public static function validateSocialName(&$element, FormStateInterface $form_state, $form) {

    $name = $element['social_name']['#value'];

    if (empty($name)) {
      return;
    }

    $is_valid = preg_match('/^[A-Za-z0-9_.-]+$/', $name);

    if ($is_valid == FALSE) {
      $form_state->setError($element['social_name'], t('The social name has illegal characters.'));
    }
  }
}
