<?php

namespace Drupal\social_name_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'social_name' field type.
 *
 * @FieldType(
 *   id = "social_name",
 *   label = @Translation("Social name"),
 *   description = @Translation("Social name"),
 *   default_widget = "social_name",
 *   default_formatter = "social_name",
 *   cardinality = 1
 * )
 */
class SocialName extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['social_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Social name'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema['columns'] = [
      'social_name' => [
        'type' => 'varchar',
        'length' => '255',
      ],
    ];

    $schema['indexes'] = [
      'social_name' => ['social_name'],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'social_url' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element = [];

    $element['social_url'] = [
      '#type' => 'url',
      '#title' => new TranslatableMarkup('Social URL'),
      '#default_value' => $this->getSetting('social_url'),
      '#size' => 60,
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    $value = $this->get('social_name')->getValue();

    return $value === NULL || $value === '';
  }

}
