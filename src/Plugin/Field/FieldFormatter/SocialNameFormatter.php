<?php

namespace Drupal\social_name_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'social_name' formatter.
 *
 * @FieldFormatter(
 *   id = "social_name",
 *   label = @Translation("Social name"),
 *   field_types = {
 *     "social_name"
 *   }
 * )
 */
class SocialNameFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $label = $this->fieldDefinition->getLabel();

    foreach ($items as $delta => $item) {

      $elements[$delta] = [
        '#theme' => 'social_name',
        '#label' => $label,
        '#social_name' => $item->social_name,
      ];
    }

    return $elements;
  }

}
